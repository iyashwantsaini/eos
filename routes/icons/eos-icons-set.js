const express = require('express')
const router = express.Router()

/* GET home page. */
router.get('/', (req, res, next) => {
  res.render('icons/eos-icons-set')
})

module.exports = router
